class MonitorPorcentaje {
  constructor(document) {
    this.tanque1 = document.getElementById("tanque1");
    this.tanque2 = document.getElementById("tanque2");
    this.tanque3 = document.getElementById("tanque3");

    this.nivel1 = document.getElementById("nivel1");
    this.nivel2 = document.getElementById("nivel2");
    this.nivel3 = document.getElementById("nivel3");

    this.nivel1.value = 0;
    this.nivel2.value = 0;
    this.nivel3.value = 0;

    this.temporizador = null;

    this.iniciarEventos();
  }

  crearGradiente(color, porcentage) {
    let gradiente = "linear-gradient(to top, " + color + " 0%, " + color + " " + porcentage + "%, white " + porcentage + "%, grey 100%)";
    
    return gradiente;
  }

  cambiarGradiente(tanque, color, porcentage) {
    console.log(this.crearGradiente(color, porcentage));
    tanque.style.background = this.crearGradiente(color, porcentage);
  }

  cambiarNiveles() {
    let evento = function() {
      this.cambiarGradiente(this.tanque1, "red", this.nivel1.value);
      this.cambiarGradiente(this.tanque2, "green", this.nivel2.value);
      this.cambiarGradiente(this.tanque3, "purple", this.nivel3.value);
    };

    return evento;
  }

  iniciarEventos() {
    console.log("Inicializando eventos");
    this.temporizador = setInterval(this.cambiarNiveles().bind(this), 5000);
  }
}

export { MonitorPorcentaje };

# Fill Css

Una de prueba de como rellenar un area/div con ayuda un gradiente css y de como usar un temporizador para hacer el cambio en forma dinamica.

## Your Project

### ← README.md

That's this file, where you can tell people what your cool website does and how you built it.

### ← index.html

Where you'll write the content of your website.

### ← style.css

CSS files add styling rules to your content.

### ← script.js

Inicializador del sitio

### ← monitorPorcentage.js

Codigo que realiza el relleno de areas

### ← assets

Drag in `assets`, like images or music, to add them to your project

## Made by [Glitch](https://glitch.com/)

\ ゜ o ゜)ノ
